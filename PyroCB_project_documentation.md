# PryoCB trajectories

## Introduction

### Known Australian pyroCB events

| Date | Description | Latitude | Longitude | Reference |
|:----------|:-------------|:-----|:------|:------|
| 18 January 2003  | [Canberra](https://www.dropbox.com/s/vjlqy01sw34u5u8/Canberra_2003.jpg?dl=0) | | | Fromm et al. (2006) | 
| 10 December 2006  | Victoria | | | |
| 14 December 2006  | Victoria | | | |
| 7 February 2009  | Victoria (Black Saturday) | | | Siddaway and Petelina (2011) |
| 2013 | [Tasmania](https://www.dropbox.com/s/vyvyqf2nps0ml35/Hobart_2012%3F.jpg?dl=0) | | | |
| 2014 | Grampians | | | | 
s
### Potential Australian pyroCB events
| Date | Description | Latitude | Longitude | Reference |
|:----------|:-------------|:-----|:------|:------|
|  1939 | (Black Friday) | | | | 
|  1961 | (Western Australian bushfires) | | | | 
|  1983 | (Ash Wednesday) | | | | 
|  1994 | (Eastern Seaboard fires) | | | | 


# 17 June 2014

* Obtained OSIRIS data for 2003, 2006 and 2009 events.
* Can use panoply to look at individual retrieval profiles. 

# 12 October

* Created bitbucket [repository](https://bitbucket.org/kstone4/pyrocb) for PyroCB code.
* Added in framework [matlab script](https://bitbucket.org/kstone4/pyrocb/src/a56e35fafc052ff87990b565e51e899cacca07ec/Save_trajectory_data_to_netcdf.m?at=master) to save trajectory data to netcdf.
* Added into markdown log to repository