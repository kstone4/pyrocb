function [] = Save_trajectory_data_to_netcdf(trajectories,output_directory)  
%framework for saving trajectory data to netcdf 

%Creating netcdf file
mode = netcdf.getConstant('NETCDF4');
mode = bitor(mode,netcdf.getConstant('CLASSIC_MODEL'));
mode = bitor(mode,netcdf.getConstant('CLOBBER'));
%mode = bitor(mode,netcdf.getConstant('NC_WRITE'));
ncid = netcdf.create(output_directory,mode);    

%creating dimensions
dimid_1 = netcdf.defDim(ncid,'dimension_1',length_of_dimension);
%create unlimited dimension
dimid_2 = netcdf.defDim(ncid,'dimension_3',netcdf.getConstant('NC_UNLIMITED'));
netcdf.getConstant

%creating variables
varid_1 = netcdf.defVar(ncid,'variable_1','double',[dimid_1 dimid_2]);

netcdf.endDef(ncid)

%writing data to previously defined variables
netcdf.putVar(ncid,varid_1,trajectories);    

netcdf.reDef(ncid)

%writing attributes
netcdf.putAtt(ncid,varid_SZA,'units','degrees')    
netcdf.putAtt(ncid,varid_SZA,'missing_value',-9999)
netcdf.putAtt(ncid,varid_SZA,'valid_min',-1)
netcdf.putAtt(ncid,varid_SZA,'valid_max',1000)

%end netcdf write
netcdf.close(ncid);

end